#include <stdio.h>
int main()
{
	int first, second, x;
	
	printf("Enter first number");
	scanf("%d",&first);
	
	printf("Enter second number");
	scanf("%d",&second);
	
	x = first;
	first = second;
	second = x;
	
	printf("First number: %d\n", first);
	printf("Second number: %d\n", second);
	
	return 0;
}
