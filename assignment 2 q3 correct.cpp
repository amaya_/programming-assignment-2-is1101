#include <stdio.h>

int main()
{
	int first, second, x;
	
	printf("Enter first number:\n");
	scanf("%d",&first);
	
	printf("Enter second number:\n");
	scanf("%d",&second);
	
	x = first;
	first = second;
	second = x;
	
	printf("first number = %d\n",first);
	printf("second number = %d\n",second);
	
	return 0;
	
}
